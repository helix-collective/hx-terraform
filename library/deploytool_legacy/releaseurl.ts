export const release_url: string =
  'https://github.com/helix-collective/hx-deploy-tool/releases/download/0.14.1/hx-deploy-tool.x86_64-linux.gz -O /opt/bin/hx-deploy-tool.gz';
